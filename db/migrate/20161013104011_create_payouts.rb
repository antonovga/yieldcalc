class CreatePayouts < ActiveRecord::Migration[5.0]
  def change
    create_table :payouts do |t|
      t.integer :month, null: false
      t.decimal :amount, null: false
      t.references :loan, foreign_key: true

      t.timestamps
    end
  end
end
