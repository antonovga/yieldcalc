class CreateLoans < ActiveRecord::Migration[5.0]
  def change
    create_table :loans do |t|
      t.decimal :rate, null: false
      t.integer :term, null: false
      t.decimal :amount, null: false
      t.decimal :delinquency_rate
      t.integer :period
      t.decimal :monthly_debt_payment
      t.decimal :monthly_interest_payment
      t.decimal :total_monthly_payment
      t.decimal :total_payment
      t.references :pool, foreign_key: true

      t.timestamps
    end
  end
end
