class CreatePools < ActiveRecord::Migration[5.0]
  def change
    create_table :pools do |t|
      t.decimal :expected_yield, default: 0
      t.decimal :actual_yield, default: 0

      t.timestamps
    end
  end
end
