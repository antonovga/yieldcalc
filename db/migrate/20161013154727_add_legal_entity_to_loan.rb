class AddLegalEntityToLoan < ActiveRecord::Migration[5.0]
  def change
    add_column :loans, :legal_entity, :string
  end
end
