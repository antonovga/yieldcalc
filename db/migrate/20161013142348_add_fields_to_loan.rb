class AddFieldsToLoan < ActiveRecord::Migration[5.0]
  def change
    add_column :loans, :interest_paid, :decimal, default: 0
    add_column :loans, :debt_paid, :decimal, default: 0
    add_column :loans, :yield, :decimal, default: 0
  end
end
