# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161013154727) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "loans", force: :cascade do |t|
    t.decimal  "rate",                                     null: false
    t.integer  "term",                                     null: false
    t.decimal  "amount",                                   null: false
    t.decimal  "delinquency_rate"
    t.integer  "period"
    t.decimal  "monthly_debt_payment"
    t.decimal  "monthly_interest_payment"
    t.decimal  "total_monthly_payment"
    t.decimal  "total_payment"
    t.integer  "pool_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.decimal  "interest_paid",            default: "0.0"
    t.decimal  "debt_paid",                default: "0.0"
    t.decimal  "yield",                    default: "0.0"
    t.string   "legal_entity"
    t.index ["pool_id"], name: "index_loans_on_pool_id", using: :btree
  end

  create_table "payouts", force: :cascade do |t|
    t.integer  "month",      null: false
    t.decimal  "amount",     null: false
    t.integer  "loan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["loan_id"], name: "index_payouts_on_loan_id", using: :btree
  end

  create_table "pools", force: :cascade do |t|
    t.decimal  "expected_yield", default: "0.0"
    t.decimal  "actual_yield",   default: "0.0"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_foreign_key "loans", "pools"
  add_foreign_key "payouts", "loans"
end
