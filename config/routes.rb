Rails.application.routes.draw do

  resources :pools, except: [:update, :edit, :new], shallow: true do
    resources :loans, except: [:index] do
      resources :payouts, except: [:index, :show]
    end
  end

  root 'pools#index'
end
