# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'yieldcalc'
set :repo_url, 'git@gitlab.com:antonovga/yieldcalc.git'

append :linked_files, 'config/database.yml', 'config/secrets.yml'
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads'
