require 'test_helper'

class PoolsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pool = pools(:first)
  end

  test "should get index" do
    get pools_url
    assert_response :success
  end

  test "should create pool" do
    assert_difference('Pool.count') do
      post pools_url
    end

    assert_redirected_to pool_url(Pool.last)
  end

  test "should show pool" do
    get pool_url(@pool)
    assert_response :success
  end

  test "should destroy pool" do
    assert_difference('Pool.count', -1) do
      delete pool_url(@pool)
    end

    assert_redirected_to pools_url
  end
end
