require 'test_helper'

class PoolTest < ActiveSupport::TestCase

  setup do
    @pool = Pool.new
  end

  test 'should respond to #actual_yield' do
    assert_respond_to @pool, :actual_yield
  end

  test 'should respond to #expected_yield' do
    assert_respond_to @pool, :expected_yield
  end
end
