# Yieldcalc


##### Ruby version

`2.3.1`

##### System dependencies

`sudo apt install libpq-dev nodejs`

##### Configuration

`cp ./config/database.yml.skel ./config/database.yml`

`cp ./config/secrets.yml.skel ./config/secrets.yml`

##### Deployment instructions

`cap production deploy`
