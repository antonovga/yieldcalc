class PoolsController < ApplicationController
  before_action :set_pool, only: [:show, :destroy]

  def index
    @pools = Pool.all.order(created_at: :desc)
  end

  def show
  end

  def create
    if params[:file].present?
      XlsxParser.new.parse_file(params[:file])
      render json: { status: 201 }
    else
      @pool = Pool.create
      redirect_to pools_url, notice: 'Pool was successfully created.'
    end
  end

  def destroy
    @pool.destroy
    redirect_to pools_url, notice: 'Pool was successfully destroyed.'
  end

  private

  def set_pool
    @pool = Pool.find(params[:id])
  end
end
