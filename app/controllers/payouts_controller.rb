class PayoutsController < ApplicationController
  before_action :set_loan, only: [:new, :create]
  before_action :set_payout, only: [:edit, :update, :destroy]

  def new
    @payout = @loan.payouts.new
  end

  def edit
  end

  def create
    @payout = @loan.payouts.new(payout_params)

    if @payout.save
      Recalculation.update_loan_values(@payout.loan)

      redirect_to loan_url(@loan), notice: 'Payout was successfully created.'
    else
      render :new
    end
  end

  def update
    if @payout.update(payout_params)
      Recalculation.update_loan_values(@payout.loan)

      redirect_to loan_url(@payout.loan), notice: 'Payout was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @loan = @payout.loan
    @payout.destroy

    redirect_to loan_url(@loan), notice: 'Payout was successfully destroyed.'
  end

  private

  def set_loan
    @loan = Loan.find(params[:loan_id])
  end

  def set_payout
    @payout = Payout.find(params[:id])
  end

  def payout_params
    params.require(:payout).permit(:month, :amount, :loan_id)
  end
end
