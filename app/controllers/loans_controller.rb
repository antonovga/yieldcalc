class LoansController < ApplicationController
  before_action :set_pool, only: [:new, :create]
  before_action :set_loan, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    @loan = @pool.loans.new
  end

  def edit
  end

  def create
    @loan = @pool.loans.new(loan_params)

    if @loan.save
      Recalculation.update_pool_values(@loan.pool)

      redirect_to pool_url(@pool), notice: 'Loan was successfully created.'
    else
      render :new
    end
  end

  def update
    if @loan.update(loan_params)
      Recalculation.update_pool_values(@loan.pool)

      redirect_to pool_url(@loan.pool), notice: 'Loan was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @pool = @loan.pool
    @loan.destroy

    redirect_to pool_url(@pool), notice: 'Loan was successfully destroyed.'
  end

  private

  def set_pool
    @pool = Pool.find(params[:pool_id])
  end

  def set_loan
    @loan = Loan.find(params[:id])
  end

  def loan_params
    params.require(:loan).permit(:amount,
                                 :delinquency_rate,
                                 :legal_entity,
                                 :period,
                                 :pool_id,
                                 :rate,
                                 :term)
  end
end
