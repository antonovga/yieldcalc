# == Schema Information
#
# Table name: pools
#
#  actual_yield   :decimal(, )      default(0.0)
#  created_at     :datetime         not null
#  expected_yield :decimal(, )      default(0.0)
#  id             :integer          not null, primary key
#  updated_at     :datetime         not null
#

class Pool < ApplicationRecord
  has_many :loans, dependent: :destroy
end
