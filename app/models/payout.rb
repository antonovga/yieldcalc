# == Schema Information
#
# Table name: payouts
#
#  amount     :decimal(, )      not null
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  loan_id    :integer
#  month      :integer          not null
#  updated_at :datetime         not null
#

class Payout < ApplicationRecord

  belongs_to :loan

  validates :amount, :month, presence: true
end
