# == Schema Information
#
# Table name: loans
#
#  amount                   :decimal(, )      not null
#  created_at               :datetime         not null
#  debt_paid                :decimal(, )      default(0.0)
#  delinquency_rate         :decimal(, )
#  id                       :integer          not null, primary key
#  interest_paid            :decimal(, )      default(0.0)
#  legal_entity             :string
#  monthly_debt_payment     :decimal(, )
#  monthly_interest_payment :decimal(, )
#  period                   :integer
#  pool_id                  :integer
#  rate                     :decimal(, )      not null
#  term                     :integer          not null
#  total_monthly_payment    :decimal(, )
#  total_payment            :decimal(, )
#  updated_at               :datetime         not null
#  yield                    :decimal(, )      default(0.0)
#

class Loan < ApplicationRecord

  belongs_to :pool
  has_many :payouts, dependent: :destroy

  validates :rate, :term, :amount, presence: true

  before_save :populate_values

  private

  def populate_values
    expected_interest_amount = amount*(rate/100.0)*(term/12.0)

    self.total_payment = amount+expected_interest_amount

    if term == 0
      self.monthly_interest_payment = 0
      self.monthly_debt_payment = 0
    else
      self.monthly_interest_payment = expected_interest_amount/term
      self.monthly_debt_payment = amount/term
    end

    self.total_monthly_payment = monthly_debt_payment+monthly_interest_payment
  end
end
