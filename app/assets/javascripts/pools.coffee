$(document).on 'ready', ->
  Dropzone.autoDiscover = false;

  if $('.dropzone').length > 0

    dropzone = new Dropzone '.dropzone', { maxFilesize: 256, maxFiles: 1 }

    dropzone.on "success", (file, response) ->
      window.location = '/pools/';

    dropzone.on 'error', (file) ->
      this.removeFile(file);
