module Recalculation
  def self.update_pool_values(pool)
    pool.expected_yield = pool.loans.average(:rate)
    pool.actual_yield = pool.loans.average(:yield)
    pool.save
  end

  def self.update_loan_values(loan)
    payouts_amount = loan.payouts.sum(:amount)
    payouts_count = loan.payouts.count

    loan.debt_paid = loan.monthly_debt_payment*payouts_count
    loan.interest_paid = payouts_amount-loan.debt_paid

    if loan.term == 0 || loan.debt_paid == 0
      loan.yield = 0
    else
      loan.yield = (loan.interest_paid/(loan.debt_paid*(loan.term/12.0)))*100
    end
    loan.save

    self.update_pool_values(loan.pool)
  end
end