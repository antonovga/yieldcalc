class XlsxParser

  PAYOUTS_ROW = 15

  def initialize
    @pool = nil
  end

  def parse_file(file_name)
    @xlsx = Roo::Spreadsheet.open(file_name)

    if @xlsx
      @pool = Pool.create

      @xlsx.sheets[1..-1].each do |sheet_name|
        parse_sheet(sheet_name)
      end
    end
  end

  private

  def parse_sheet(sheet_name)
    sheet = @xlsx.sheet(sheet_name)

    loan = parse_loan(sheet, sheet_name)

    if loan.persisted?
      parse_payouts(sheet, loan)
    end
  end

  def parse_loan(sheet, sheet_name)
    loan = @pool.loans.new

    loan.amount = sheet.b2
    loan.term = sheet.b3
    loan.rate = sheet.b4*100
    loan.period = sheet.b5
    loan.delinquency_rate = sheet.b6*100
    loan.legal_entity = sheet_name

    loan.save

    Recalculation.update_pool_values(loan.pool)

    loan
  end

  def parse_payouts(sheet, loan)
    sheet.row(PAYOUTS_ROW).each_with_index do |payout, index|
      loan.payouts.create(month: (index+1), amount: payout)

      Recalculation.update_loan_values(loan)
    end
  end
end